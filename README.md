MIUI 10 Android 9.0 Port for OnePlus 5

ROM Base: OxygenOS Beta 35
ROM Port: MIUI 10 Global 9.6.27 for MI6 (same chipset)

This port will be guided according to @jhenrique09 MIUI Porter port (https://gitlab.com/jhenrique09/miui_potter). Big thanks to him for making a clean and useful port process
